import { useEffect, useRef, useState } from 'react'
import "./App.css"

// todo: support non wav, cleanup
// future: resampling

const THRESHHOLD = 0.1
const RESIZE_DEBOUNCE_TIME = 200

const DEFAULT_DECAY_TIME = 0.2
const DEFAULT_BRIGHTNESS = 0.2
const DEFAULT_BG_COLOR: [number, number, number] = [0, 0, 0]
const DEFAULT_FG_COLOR: [number, number, number] = [0, 255, 0]
const DEFAULT_FPS = 60

interface CanvasProps {
  audioBuffer: ArrayBuffer | undefined,
  audio: HTMLAudioElement | undefined,
  // sampleScale: [number, number]
}

const toHexColor = (r: number, g: number, b: number) => {
  return ((((255 << 8) + b << 8) + g) << 8) + r
}

const calcDelay = (t: number, fps: number) => { 
  return 1 - Math.pow(THRESHHOLD, 1 / (t * fps))
}

const App: React.FC = () => {
  const [audio, setAudio] = useState<HTMLAudioElement>()
  const [audioBuffer, setAudioBuffer] = useState<ArrayBuffer>()

  const loadFile = (event: React.ChangeEvent<HTMLInputElement>) => {
    if (event.target.files != null) {
      let file = event.target.files[0]
      let reader = new FileReader()
      if (file) {
        reader.readAsArrayBuffer(file)
        reader.onload = (event: ProgressEvent<FileReader>) => {
          const result = event.target?.result
          if (result instanceof ArrayBuffer) {
            setAudioBuffer(result)
            setAudio(new Audio(URL.createObjectURL(file)))
          }
        }
      }
    }
  }

  return (
    <div className='app' >
      <Canvas audioBuffer={audioBuffer} audio={audio} ></Canvas>
      <input type='file' onChange={loadFile} />
    </div>
  )
}

const Canvas: React.FC<CanvasProps> = (props) => {
  const canvasRef = useRef<HTMLCanvasElement>(null)
  const ctxRef = useRef<CanvasRenderingContext2D>()
  const imageViewRef = useRef<Uint32Array>()
  const imageDataRef = useRef<ImageData>()
  const audioViewRef = useRef<Int16Array>()
  const [width, setWidth] = useState<number>(0)
  const [height, setHeight] = useState<number>(0)
  const [playing, setPlaying] = useState<Boolean>(false)
  const [fps, setFps] = useState<number>(DEFAULT_FPS)
  const [sampleRate, setSampleRate] = useState<number>(0)
  const [samplesPerFrame, setSamplesPerFrame] = useState<number>(0)
  const intensityRef = useRef<Map<number, number>>(new Map())
  const savedTimeRef = useRef<number>(0)
  const frameNumRef = useRef<number>(0) 
  const [decayTime, setDecayTime] = useState<number>(DEFAULT_DECAY_TIME)
  const [decay, setDecay] = useState<number>(0)
  const [fgColor, setFgColor] = useState<[number, number, number]>(DEFAULT_FG_COLOR)
  const [bgColor, setBgColor] = useState<[number, number, number]>(DEFAULT_BG_COLOR)
  const [brightness, setBrightness] = useState<number>(DEFAULT_BRIGHTNESS)

  const togglePlaying = () => {
    if (props.audio) {
      if (props.audio.paused) {
        props.audio.play().then(() => {
          setPlaying(true)
        }, () => {
          setPlaying(false)
        })
      } else {
        props.audio.pause()
      }
    }
  }

  useEffect(() => {
    let frameRef: number
    const update = (time: number) => {
      frameNumRef.current++
      if (frameNumRef.current === fps) {
        frameNumRef.current = 0
        if (savedTimeRef.current) {
          const fpsCalc = Math.round(fps * 1000 / (time - savedTimeRef.current))
          if (fpsCalc && fpsCalc !== fps) {
            setFps(fpsCalc)
          }
        }
        savedTimeRef.current = time
      }
      
      // calc image
      if (props.audio && playing) {
        const imageView = imageViewRef.current!
        const audioView = audioViewRef.current!
        const start = Math.floor(props.audio.currentTime * sampleRate)

        // decay
        const intensity = intensityRef.current
        for (let [key, value] of intensity) {
          intensity.set(key, value * (1 - decay))
        }
        // update intensity
        const size = Math.min(width, height)
        const xDiff = (width - size) / 2
        const yDiff = (height - size) / 2
        const scale = size / 65535
        let sum = 0
        for (let i=0; i<samplesPerFrame; i++) {
          const curr = (i + start) * 2
          const x = Math.round((audioView[curr] + 32768) * scale + xDiff)
          const y = Math.round((-audioView[curr + 1] + 32768) * scale + yDiff)
          
          for (let j=0; j<101; j++) {
            sum += audioView[j] * audioView[i+j]
          }
          if (x >= 0 && x < width && y >= 0 && y < height) {
            const idx = (y * width + x)
            if (intensity.has(idx)) {
              intensity.set(idx, Math.min(intensity.get(idx)! + brightness, 1))
            } else {
              intensity.set(idx, brightness)
            }
          }
        }

        // apply
        let minX = width - 1
        let maxX = 0
        let minY = height - 1
        let maxY = 0
        for (let [key, value] of intensity) {
          const x = key % width
          const y = Math.floor(key / width)
          if (x > maxX) maxX = x
          if (x < minX) minX = x
          if (y > maxY) maxY = y
          if (y < minY) minY = y
          if (value < THRESHHOLD) {
            imageView[key] = toHexColor(bgColor[0], bgColor[1], bgColor[2])
            intensity.delete(key)
          } else {
            const inverseValue = 1 - value
            const r = Math.round(value * fgColor[0] + inverseValue * bgColor[0])
            const g = Math.round(value * fgColor[1] + inverseValue * bgColor[1])
            const b = Math.round(value * fgColor[2] + inverseValue * bgColor[2])
            imageView[key] = toHexColor(r, g, b)
          }
        }
        // put image
        if (imageDataRef.current && ctxRef.current) {
          ctxRef.current.putImageData(imageDataRef.current, 0, 0, minX, minY, maxX - minX + 1, maxY - minY + 1)
        }
      }
      
      // request next frame
      frameRef = requestAnimationFrame(update)
    }

    frameRef = requestAnimationFrame(update)

    return () => {
      if (frameRef) {
        cancelAnimationFrame(frameRef)
      }
    }
  }, [width, height, playing, props.audio, fps, sampleRate, samplesPerFrame, fgColor, bgColor, brightness, decay])

  useEffect(() => {
    if (width && height) {
      const buffer = new ArrayBuffer(width * height * 4)
      imageViewRef.current = new Uint32Array(buffer)
      imageDataRef.current = new ImageData(new Uint8ClampedArray(buffer), width, height)
      const bgColorHex = toHexColor(...bgColor)
      const imageView = imageViewRef.current
      if (imageView) {
        for (let i=0; i<imageView.length; i++) {
          imageView[i] = bgColorHex
        }
      }
      if (imageDataRef.current && ctxRef.current) {
        ctxRef.current.putImageData(imageDataRef.current, 0, 0)
      }
    }
  }, [width, height, bgColor])
  
  useEffect(() => {
    if (sampleRate) {
      const samplesPerFrame = Math.round(sampleRate / fps)
      setSamplesPerFrame(samplesPerFrame)
    }
  }, [fps, sampleRate])

  useEffect(() => {
    let debounceTimer: NodeJS.Timeout
    const onResize = () => {
      clearTimeout(debounceTimer)
      debounceTimer = setTimeout(() => {
        if (canvasRef.current) {
          const rect = canvasRef.current.getBoundingClientRect()
          setWidth(Math.round(rect.width))
          setHeight(Math.round(rect.height))
        }
      }, RESIZE_DEBOUNCE_TIME)
    }
    window.addEventListener('resize', onResize)

    if (canvasRef.current) {
      const ctx = canvasRef.current.getContext('2d', {alpha: false})
      if (ctx) {
        ctxRef.current = ctx
      } else {
        // handle error
      }
      const rect = canvasRef.current.getBoundingClientRect()
      setWidth(Math.round(rect.width))
      setHeight(Math.round(rect.height))
    } else {
      // handle error
    }
    return () => {
      window.removeEventListener('resize', onResize)
    }
  }, [])

  useEffect(() => {
    if (props.audio) {
      props.audio.onpause = () => setPlaying(false)
      props.audio.onplay = () => setPlaying(true)
      props.audio.play().then(() => {
        setPlaying(true)
      }, () => {
        setPlaying(false)
      })
    }
    
    if (props.audioBuffer) {
      audioViewRef.current = new Int16Array(props.audioBuffer, 44)
      const tempView = new Uint16Array(props.audioBuffer, 24)
      setSampleRate(tempView[0] + (tempView[1] << 16))
      console.log(tempView[0] + (tempView[1] << 16))
    }
    return () => {
      props.audio?.pause()
    }
  }, [props.audio, props.audioBuffer])

  useEffect(() => {
    setDecay(calcDelay(decayTime, fps))
  }, [fps, decayTime])

  return (
    <div>
      <div className='container' >
        <canvas className='canvas' width={width} height={height} ref={canvasRef} onClick={togglePlaying}/>
        <button className='play-button' onClick={togglePlaying}>
          <i className='material-icons' style={{color: 'white'}}>{playing ? 'pause' : 'play_arrow'} </i>
        </button>
      </div>
      <p>{fps}</p>
    </div>
  )
}

export default App;
