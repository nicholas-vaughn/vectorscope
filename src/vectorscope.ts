const audioContext = new AudioContext()

export interface Point {
    x: number,
    y: number
}


export function calcPoints(buffer: ArrayBuffer, callBack: (points: Array<Point>) => void,
                           width: number, height: number) {
    audioContext.decodeAudioData(buffer).then((buffer: AudioBuffer) => {
        let points = new Array<Point>(buffer.length)
        const xs = buffer.getChannelData(0)
        const ys = buffer.getChannelData(1)
        const scale = Math.min(width, height)
        for (let i=0; i<points.length; i++) {
        let x = (xs[i] * scale + width) / 2
        let y = (-ys[i] * scale + height) / 2
        points[i] = {x: x, y: y}
        }
        callBack(points)
    })
}

export function processWav(buffer: ArrayBuffer) {
    const arr = new Uint8Array(buffer)
    const chunkId = String.fromCharCode(arr[0], arr[1], arr[2], arr[3])
    const audioFormat = arr[20] + (arr[21] << 8)
    const sampleRate = arr[24] + (arr[25] << 8) + (arr[26] << 16) + (arr[27] << 24)
    const numChannels = arr[22] + (arr[23] << 8)
    const bitsPerChannel = arr[34] + (arr[35] << 8)
    if (chunkId === "RIFF" && audioFormat === 1 && numChannels === 2 && 
        (bitsPerChannel === 8 || bitsPerChannel === 16)) {
        if (bitsPerChannel === 8) {
            // handle 8 bit
        } else {
            // handle 16 bit
        }
    } else {
        // unsupported audio format
    }
}